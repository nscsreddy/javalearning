import java.io.Closeable;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A {@link Closeable} that never throws an Exception when closing.
 * Note that closing must be idempotent.
 */
public interface SafeCloseable extends Closeable {

    @Override
    void close();

    static SafeCloseable makeIdempotent(Runnable runOnlyOnceOnClose) {
        return new SafeCloseable() {

            private final AtomicBoolean ran = new AtomicBoolean(false);

            @Override
            public void close() {
                if (ran.compareAndSet(false, true)) {
                    runOnlyOnceOnClose.run();
                }
            }
        };
    }
}
