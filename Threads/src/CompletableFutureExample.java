import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CompletableFutureExample {

	public static CompletableFutureTest cft = new CompletableFutureTest();
	public static int onum=0;
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
//			ExecutorService cpuBound = Executors.newFixedThreadPool(4);
			ExecutorService ioBound = Executors.newCachedThreadPool();
			onum++;
			CompletableFuture.supplyAsync(() -> cft.placeOrder(onum), ioBound)
			.thenApply(order -> cft.enhanceOrder(order))
			.thenApply(order -> cft.processPayment(order))
			.thenApply(order -> cft.dispatchOrder(order))
			.thenApply(order -> cft.sendEmail(order));
		}
		
//			ExecutorService ioBound = Executors.newCachedThreadPool();
//			onum++;
//			CompletableFuture.supplyAsync(() -> cft.placeOrder(onum), ioBound)
//			.thenApply(order -> cft.enhanceOrder(onum))
//			.thenApplyAsync(order -> cft.processPayment(onum))
//			.thenApply(order -> cft.dispatchOrder(onum))
//			.thenApply(order -> cft.sendEmail(onum));
	}
}
