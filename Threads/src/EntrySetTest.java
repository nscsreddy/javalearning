import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EntrySetTest {
	static SingleThreadGuard guard = new SingleThreadGuard();

	public void concurrenctTestMethod() {
//		try (SafeCloseable sc = guard.enter("concurrenctTestMethod")) {
			System.out.println("guard = "+guard);
			
//			Map<String, String> map1 = new ConcurrentHashMap<>();

		Map<String, String> map1 = new HashMap<>();
			map1.put("A", "A Value");
			map1.put("b", "b Value");
			map1.put("c", "c Value");
			map1.put("d", "d Value");
			map1.put("e", "e Value");

			map1.entrySet().stream().forEach(e -> {
				System.out.println(e.getKey());
				System.out.println(e.getValue());

			});
			Map<String, String> map2 = new HashMap<>();
			map2.putAll(map1);
			
			System.out.println(map1);
			System.out.println(map2);

//			map1.entrySet().stream().forEach(e -> {
//				System.out.println(e.getKey());
//				System.out.println(e.getValue());
//
//			});
//			System.out.println(map1.containsValue("x Value"));
		}
//	}
}
