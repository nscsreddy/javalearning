import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class TestClassAsKey {
	public static void main(String[] args) {
		HashMap<User, String> map = new HashMap<User, String>();

		User a1 = new User(1);
		a1.setUserName("A_ONE");

		User a2 = new User(2);
		a2.setUserName("A_TWO");

		User a3 = new User(3);
		a3.setUserName("B_THREE");

		User a4 = new User(4);
		a4.setUserName("B_FOUR");

		User a5 = new User(5);
		a5.setUserName("C_FIVE");

		map.put(a1, a1.getUserName());
		map.put(a2, a2.getUserName());
		map.put(a3, a3.getUserName());
		map.put(a4, a4.getUserName());
		map.put(a5, a5.getUserName());

//		Set<User> keys = map.keySet().stream().filter(s -> s.getUserName().startsWith("d_"))
//				.collect(Collectors.toSet());

		map.keySet().stream().forEach(e -> {
			if (e.getUserName().startsWith("") && map.get(e).startsWith("A_")) {
				System.out.println(map.get(e));
			}
		});

//				.filter(s -> s.getUserName().startsWith("d_"))
//				.collect(Collectors.toSet());

//		map.keySet().removeAll(keys);
//
//		for (User key : keys) {
//			System.out.println(map.get(key));
//		}
//		System.out.println(map); 

		Map<String, String> map2 = new HashMap<>();
		map2.put("a", "");
		map2.put("b", "");
		map2.put("c", "");

		Set<String> set = new HashSet<>();
//		set.add("a");
//		set.add("b");

		map2.keySet().removeAll(set);

		System.out.println(map2);

//		final String uuid = UUID.randomUUID().toString();

		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());
		System.out.println(UUID.randomUUID().toString());

	}
}