
import java.util.ConcurrentModificationException;


public class SingleThreadGuard {

//    private static final Logger logger = LogManager.getLogger(SingleThreadGuard.class);

    private final SafeCloseable closer = this::leave;
    private String currentMethod;

    public SafeCloseable enter(String methodName) {
    	System.out.println("Entering "+ methodName);
    	String threadName = Thread.currentThread().getName();
    	System.out.println("Thread Name "+ threadName);
        if (currentMethod != null) {
            throw new ConcurrentModificationException("Method " + currentMethod + " already active");
        }
        currentMethod = methodName;
        return closer;
    }

    private void leave() {
    	System.out.println("Leaving "+ currentMethod);
        if (currentMethod == null) {
            throw new ConcurrentModificationException("Left twice");
        }
        currentMethod = null;
    }
}
