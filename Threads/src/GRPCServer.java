
public class GRPCServer {
	class MyThread extends Thread {
		public void run() {
			System.out.println("Inside MyThread");
		}
	}

	public static void main(String[] args) {
		System.out.println("Inside Main Before Thread Start");

		MyThread st = new GRPCServer().new MyThread();
		Thread t = new Thread(st);
		t.start();
		System.out.println("Inside Main After Thread Start");

	}

}
