public class ThreadExample extends Thread {
	ThreadExampleData data = new ThreadExampleData(new EntrySetTest());
	public static void main(String[] args) {
		ThreadExample thread2 = new ThreadExample();
		ThreadExample thread1 = new ThreadExample();
		ThreadExample thread3 = new ThreadExample();
		ThreadExample thread4 = new ThreadExample();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
	}
	
	public void run() {
		data.entrySetTest.concurrenctTestMethod();
	}
}