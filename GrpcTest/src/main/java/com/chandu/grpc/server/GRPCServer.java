package com.chandu.grpc.server;


import com.chandu.grpc.products.ProductService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GRPCServer {
	class MyGRPCServerThread extends Thread {
		Server inThreadserver;

		public Server getInThreadserver() {
			return inThreadserver;
		}

		public void setInThreadserver(Server inThreadserver) {
			this.inThreadserver = inThreadserver;
		}

		public void run() {
			try {
				server.start();
				System.out.println("Server grpc Started in MyGRPCServerThread on Port: " + server.getPort());
				System.out.println("Grpc server is awaitTermination");
				server.awaitTermination();
				System.out.println("AFTER awaitTermination");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	static Server server;

	public static void main(String[] args) {
		System.out.println("Starting grpc server\n");
		new GRPCServer().startServer();
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("About to Shutdown the Server\n");
//		server.shutdown();
//		System.out.println("Server Shutdown complete\n");
	}

	public void startServer() {
		server = ServerBuilder.forPort(9089).addService(new ProductService()).build(); // create a instance
		MyGRPCServerThread st = new MyGRPCServerThread();
		st.setInThreadserver(server);
		Thread t = new Thread(st);
		t.start();
	}
}
