import com.chandu.grpc.User.APIResponse;
import com.chandu.grpc.User.LoginRequest;
import com.chandu.grpc.userGrpc;
import com.chandu.grpc.userGrpc.userBlockingStub;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClient {

	public static void main(String[] args) {

		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();

		// stubs - generate from proto

		userBlockingStub userStub = userGrpc.newBlockingStub(channel);

		LoginRequest loginrequest = LoginRequest.newBuilder().setUsername("RAM").setPassword("RAM").build();

		APIResponse response = userStub.login(loginrequest);

		System.out.println("Response = " + response.getResponsemessage());
//		channel.shutdown();

		loginrequest = LoginRequest.newBuilder().setUsername("RAM1").setPassword("RAM").build();

		response = userStub.login(loginrequest);

		System.out.println("Response = " + response.getResponsemessage());
//		channel.shutdown();

//		System.out.println("Trying to Login after channel shutdown");
		loginrequest = LoginRequest.newBuilder().setUsername("RAM").setPassword("RAM").build();

		response = userStub.login(loginrequest);

		System.out.println("Response = " + response.getResponsemessage());
	}

}
