package com.chandu.grpc.server;

import java.util.LinkedList;
import java.util.Queue;

public class Example {
	public static void main(String[] args) {
		Queue<Integer> q = new LinkedList<>();
		for(int i=1;i<101;i++) {
			q.add(i);
		}
		System.out.println("The queue is: " + q);
		for(int i=1;i<51;i++) {
			System.out.println("Element removed from Queus " + q.remove());
		}
		System.out.println("The queue after deletion is: " + q);
		int head = q.peek();
		System.out.println("The head of the queue is: " + head);
		int size = q.size();
		int lastElement = q.poll();
		System.out.println("The size of the queue is: " + lastElement);
	}
}