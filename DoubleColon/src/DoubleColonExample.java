import java.util.ArrayList;
import java.util.List;

public class DoubleColonExample {

	/*
	 * When and how to use double colon operator?
	 * 
	 * Method reference or double colon operator can be used to refer:
	 * 
	 * a static method, an instance method, or a constructor. How to use method
	 * reference in Java: 
	 * 1. Static method Syntax:
	 * 
	 * (ClassName::methodName)
	 */

	/*
	 * 2. Instance method Syntax:
	 * 
	 * (objectOfClass::methodName) Example:
	 * 
	 * System.out::println
	 * 
	 * 3. Super method Syntax:
	 * 
	 * (super::methodName)
	 * 
	 * 4. Instance method of an arbitrary object of a particular type Syntax:
	 * 
	 * (ClassName::methodName) Example:
	 * 
	 * SomeClass::someInstanceMethod
	 * 
	 * 5. Class Constructor Syntax:
	 * 
	 * (ClassName::new) Example:
	 * 
	 * ArrayList::new
	 */

	// static function to be called
	static void someFunction(String s) {
		System.out.println(s);
	}

	void someOtherFunction(String s) {
		System.out.println(s);
	}

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.add("Geeks");
		list.add("For");
		list.add("GEEKS");

		// call the static method
		// using double colon operator
		list.forEach(DoubleColonExample::someFunction);

		// call the instance method
		// using double colon operator
		list.forEach((new DoubleColonExample())::someOtherFunction);

	}
}
