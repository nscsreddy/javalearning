import java.util.ArrayList;
import java.util.function.Consumer;

public class LamdaFunctions {
	/*
	 * The simplest lambda expression contains a single parameter and an expression:
	 * 
	 * parameter -> expressionv
	 */
	/*
	 * To use more than one parameter, wrap them in parentheses:
	 * 
	 * (parameter1, parameter2) -> expression
	 */
	/*
	 * In order to do more complex operations, a code block can be used with curly
	 * braces. If the lambda expression needs to return a value, then the code block
	 * should have a return statement.
	 * 
	 * (parameter1, parameter2) -> { code block }
	 * 
	 */

	interface StringFunction {
		String run(String str);
	}

	public static void main(String[] args) {

		StringFunction exclaim = (s) -> s + "!";
		StringFunction ask = (s) -> s + "?";
		printFormatted("Hello", exclaim);
		printFormatted("Hello", ask);

		/*************/
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		numbers.add(5);
		numbers.add(9);
		numbers.add(8);
		numbers.add(1);
		Consumer<Integer> method = (n) -> {
			System.out.println("Chandu " + n);
		};
		numbers.forEach(method);

	}

	public static void printFormatted(String str, StringFunction format) {
		String result = format.run(str);
		System.out.println(result);
	}

}
