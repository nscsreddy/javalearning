
public class GenericsTest {
	public static void main(String[] args) {
		Generics<Integer, String> g1 = new Generics<Integer, String>(10, "Chandu");
		Generics<Thread, String> g2 = new Generics<>(new Thread(), "Chandu");
		g1.ShowType();
		g2.ShowType();

		int num = 5;
		NumberFunctions<Number> nf = new NumberFunctions<>(num);
		System.out.println("Square of " + num + " = " + nf.square());

		NumberFunctions<Integer> in = new NumberFunctions<Integer>(10);
		NumberFunctions<Double> dob = new NumberFunctions<Double>(-10.0);

		System.out.println(in.absoluteValue(dob));

	}
}
