
/**
 * Instance of Generics class below can be created by passing any to object types.
 * Thus the use of generics helps code reusability.
 * @author sekhar
 *
 * @param <T>
 * @param <V>
 */
public class Generics<T,V> {
	T obj;
	V obj1;

	public Generics(T obj, V obj1) {
		this.obj = obj;
		this.obj1 = obj1;
	}
	
	void ShowType() {
		System.out.println(this.obj.getClass().getName());
		System.out.println(this.obj1.getClass().getName());
	}

}
