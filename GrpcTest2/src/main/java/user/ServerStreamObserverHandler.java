package user;

import com.chandu.grpc.User.APIResponse;

import io.grpc.stub.StreamObserver;

public class ServerStreamObserverHandler implements StreamObserver<APIResponse> {

//    private static final Logger logger = LogManager.getLogger(ServerStreamObserverHandler.class);
//    private static final String COMPONENT_NAME = Sync2CloudService.class.getName();
	private final StreamObserver<APIResponse> clientObserver;

	public ServerStreamObserverHandler(StreamObserver<APIResponse> clientObserver) {
		this.clientObserver = clientObserver;
	}

	@Override
	public void onNext(APIResponse value) {
		System.out.println("Server On Next Called");
//        LagSimulator.mayLag("sync2.streamObserver.onNext", value);
//
//        if (value.hasEmbeddedInfo()) {
//            registerToServer(value.getEmbeddedInfo());
//        } else {
//            logger.atDebug().withMarker(LogMarkers.MARKER_SYNC_IN).log("Acknowledgement from Embedded platform");
//
//            Acknowledgement syncConfirmation = value.getAcknowledgement();
//            String messageId = syncConfirmation.getMessageId();
//
//            if (!BroadcastNotification.isAcknowledgeTrackingPresent(messageId)) {
//                // TODO unknown message ID
//                logger.atError().withMarker(LogMarkers.MARKER_SYNC).log("Unknown message ID {} received!", messageId);
//            } else if (syncConfirmation.getSuccess()) {
//
//                List<MessageTrackingInfo> messageTrackingInfos = BroadcastNotification
//                        .removeAcknowledgementTracking(messageId);
//                messageTrackingInfos.forEach(messageTrackingInfo -> {
//                    SyncedInfoKeyCloud syncedInfoKeyImpl = messageTrackingInfo.getSyncedInfoKeyImpl();
//                    logger.atInfo()
//                            .withMarker(LogMarkers.MARKER_SYNC_IN)
//                            .log("Acknowledgement from Embedded platform {} on successful sync uri {} for user {}",
//                                    syncedInfoKeyImpl.getRemotePlatformId(), messageTrackingInfo.getUri(),
//                                    syncedInfoKeyImpl.getBlackboard());
//                    //storage of sync files for handling and comparision with deleted files - GOS-1769
//                    if (messageTrackingInfo.getOperation() == SyncOperationType.CREATE) {
//                        BroadcastNotification.syncedInfo.onCreate(syncedInfoKeyImpl, messageTrackingInfo.getUri());
//                    } else if (messageTrackingInfo.getOperation() == SyncOperationType.DELETE) {
//                        BroadcastNotification.syncedInfo.onDelete(syncedInfoKeyImpl, messageTrackingInfo.getUri());
//                    }
//                });
//            } else if (!syncConfirmation.getSuccess()) {
//                logger.atDebug().withMarker(LogMarkers.MARKER_SYNC).log("Message with ID {} ACK!", messageId);
//                //when it fails the sync in remote platform due to unknown reasons and we decide the remove message info
//                BroadcastNotification.removeAcknowledgementTracking(messageId);
//            }
//
//            // TODO what do?
//            /*            final List<String> timedOutMessages = BroadcastNotification.messagesIds.entrySet()
//                    .stream()
//                    .filter(e -> e.getValue().isBefore(Instant.now().minusSeconds(300)))
//                    .map(Map.Entry::getKey)
//                    .collect(Collectors.toList());
//            
//            timedOutMessages.forEach(mId -> logger.atError()
//                    .withMarker(LogMarkers.MARKER_SYNC)
//                    .log("Message with ID {} timed out! (older than 5min)", mId));
//                    */
//        }
//
//    
	}

	@Override
	public void onError(Throwable t) {
//    	
//        LagSimulator.mayLag("sync2.streamObserver.onError", t);
//        logger.atWarn()
//                .withThrowable(t)
//                .withMarker(LogMarkers.MARKER_SYNC)
//                .log("onError registerToServer - Disconnected");
//        //When gRPC server throws the error while communicating with client
//        if (t instanceof StatusRuntimeException) {
//            logger.info("Stream observer disconnected due to server connectivity issues");
//
//            StatusRuntimeException stre = (StatusRuntimeException) t;
//            if (!stre.getMessage().equals(SyncStatusMessage.SHUTDOWN_NOW.getMessage())) {
//                logger.info("gRPC channel connectivity issue - Status.UNAVAILABLE");
//            }
//
//            /*stateMachineHolder.getStateMachine(blackboardId)
//                    .handleNetworkFailure(SyncStatusMessage.getByMessage(stre.getMessage()));*/
//        }
//    
	}

	@Override
	public void onCompleted() {
//        LagSimulator.mayLag("sync2.streamObserver.onCompleted", null);
//        logger.debug("onCompleted registerToServer.");
//
//        clientObserver.onCompleted();
	}

//    /**
//     * User registration with platform Cloud, called when User is created in Embedded platform
//     */
//    public void registerToServer(EmbeddedInfo request) {
//        LagSimulator.mayLag("sync2.registerToServer", request);
//        try (ContextRestorer restorer = GrpcContextConverter.activateContext(request.getContext(), COMPONENT_NAME)) {
//            String userId = request.getUserId();
//            ClientInfo<CloudResponseEnvelope> clientInfo = new ClientInfo<>(request.getEmbeddedId(), clientObserver);
//            BroadcastNotification.clientList.addClient(userId, clientInfo);
//            logger.info(LogMarkers.MARKER_SYNC_IN, "Platform {} is registered for user {}", request.getEmbeddedId(),
//                    userId);
//        }
//    }
}
