
public class CompletableFutureTest {
	public int placeOrder(int onum) {
		System.out.println(" Placed Order " + onum);
		return onum;
	}

	public int enhanceOrder(int onum) {
		System.out.println(" Enhanced Order " + onum);
		return onum;
	}

	public int processPayment(int onum) {
		System.out.println(" Processed Order " + onum);
		return onum;

	}

	public int dispatchOrder(int onum) {
		System.out.println(" Dispatched Order " + onum);
		return onum;

	}

	public int sendEmail(int onum) {
		System.out.println(" Sent Email " + onum);
		return onum;
	}

}
