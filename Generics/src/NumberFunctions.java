/**
 * Specifying the super class of the generic type T allows us the use the
 * funtions on that super class
 * 
 * @author sekhar
 *
 * @param <T>
 */
public class NumberFunctions<T extends Number> {
	T num;

	public NumberFunctions(T obj) {
		this.num = obj;
	}

	double square() {
		return num.intValue() * num.doubleValue();
	}

	/**
	 * The below tow instances of NumberFunctions object are created with Integer and Double.
	 * so when absoluteValue is called on in object then it expects dob also tobe of Integer.
	 * To avoid errors we use wild cards
	 * NumberFunctions<Integer> in = new NumberFunctions<Integer>(10);
	 * NumberFunctions<Double> dob = new NumberFunctions<Double>(-10.0);
	 * in.absoluteValue(dob);
	 * 
	 * @param obj1
	 * @return
	 */
	boolean absoluteValue(NumberFunctions<?> obj1) {
		if (Math.abs(num.doubleValue()) == Math.abs(obj1.num.doubleValue()))
			return true;
		else
			return false;
	}
}
