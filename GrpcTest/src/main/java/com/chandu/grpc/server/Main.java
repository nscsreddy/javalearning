package com.chandu.grpc.server;

public class Main extends Thread {
	public static int amount = 0;

	public static void main(String[] args) {
		Main thread = new Main();
		thread.start();
		// Wait for the thread to finish
		while (thread.isAlive()) {
			System.out.println("Waiting...");
		}
		amount++;
		// Update amount and print its value
		System.out.println("Main: " + amount);
	
		
	}

	public void run() {
		amount++;
		System.out.println("Amount in run: " + amount);
	}
}
